// Creat new array DSNV 
var dsnv = []

// check if there is data in local storage 
const DSNV_LOCAL = 'DSNV_LOCAL';
var jsonData = localStorage.getItem('DSNV_LOCAL')
if(jsonData != null){
    dsnv = JSON.parse(jsonData);
    renderDSNV(dsnv);
}

function layThongTinTuForm() {
    var taiKhoan = document.getElementById("tknv").value ;
    var ten = document.getElementById("name").value ;
    var email = document.getElementById("email").value ;
    var matKhau = document.getElementById("password").value ;
    var ngayLam = document.getElementById("datepicker").value ;
    var salary = document.getElementById("luongCB").value *1;
    var chucVu = document.getElementById("chucvu").value ;
    var gioLam = document.getElementById("gioLam").value*1;

    var nv = {
        taiKhoan: taiKhoan,
        ten: ten,
        email: email,
        matKhau: matKhau,
        ngayLam: ngayLam,
        salary: salary,
        chucVu: chucVu,
        gioLam: gioLam,
    }
    return nv
}
console.log("nv", layThongTinTuForm())
function renderDSNV(dsnv) {
    var contentHTML = "";
    for (var i =0; i<dsnv.length; i++){
        var item = dsnv[i]
        var contentTr = 
        `
        <tr>
            <td>${item.taiKhoan}</td>
            <td>${item.ten}</td>
            <td>${item.email}</td>
            <td>${item.ngayLam}</td>
            <td>${item.chucVu}</td>
            <td>${tinhLuong(item.salary)} </td>
            <td>0</td>
            <td>
                <button onclick="xoaNV('${item.taiKhoan}')" class="btn btn-danger">Delete</button>
                <button class="btn btn-info">Change</button>
            </td>
        </tr>
        `
        contentHTML += contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function themNV() {
    var nv = layThongTinTuForm();
    dsnv.push(nv);
    renderDSNV(dsnv);
    
    //Save data to local storage
    var jsonData = JSON.stringify(dsnv);
    localStorage.setItem('DSNV_LOCAL', jsonData);

}


function tinhLuong(salary) {
    var netSalary;

    switch(document.getElementById("chucvu").value){
        case "A":
            netSalary = salary * 3;
            break
        case "B":
            netSalary = salary * 2;
            break
        case "C":
            netSalary = salary;
            break     
    }

    return netSalary
}


function xoaNV(id){

    var viTri = -1;
    for(var i=0;i<dsnv.length;i++){
        if(dsnv[i].taiKhoan == id){
            viTri = i;
        }
    }
    if (viTri != -1){
        
        dsnv.splice(viTri, 1);
        renderDSNV(dsnv);
    }
    // ==*Save data to local storage*== 
    let dataJson = JSON.stringify(dsnv);
    localStorage.setItem('DSNV_LOCAL', dataJson)
}

